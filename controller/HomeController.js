class Home {
  // View
  static sendView(req, res, next) {
    const payload = {
      name: "kevin",
      address: "earth",
      friends: ["aldo", "aksa", "jr"]
    }
    res.render("index", payload)
  }

  static home(req, res, next) {
    let data = {
      login: false,
      name: "",
    }
    if (req.isAuthenticated()) {
      data = {
        login: req.isAuthenticated(),
        name: req.user.dataValues.username,
      }

    }
    res.render("home", data)
  }
  static game(req, res, next) {
    const data = {
      name: req.user.dataValues.username,
      user_id: req.user.dataValues.id,
    }
    res.render("game", data)
  }

}

module.exports = Home