'use strict';

const bcrypt = require('bcrypt')

module.exports = {
  // async up(queryInterface, Sequelize) {

  //   await queryInterface.bulkInsert('user_games', [
  //     {
  //       username: 'harlansr',
  //       password: 'password',
  //     },
  //     {
  //       username: 'dummy',
  //       password: 'password',
  //     },
  //   ], {});
  // },

  // async down(queryInterface, Sequelize) {
  //   await queryInterface.bulkDelete('user_games', null, {});
  // }

  up: async (queryInterface) => {
    await queryInterface.bulkInsert('user_games', [
      { username: 'admin', password: bcrypt.hashSync('password', 10), admin: true },
      { username: 'user', password: bcrypt.hashSync('password', 10), admin: false },
      { username: 'harlansr', password: bcrypt.hashSync('password', 10), admin: false },
    ], {});

    const user_game = await queryInterface.sequelize.query(
      `SELECT id from user_games;`
    );

    const courseRows = user_game[0];

    return await queryInterface.bulkInsert('user_game_biodata', [
      { name: 'Admin', email: 'admin@gmail.com', user_game_id: courseRows[0].id },
      { name: 'Dummy', email: 'dummy@gmail.com', user_game_id: courseRows[1].id },
      { name: 'Harlan SR', email: 'harlan@gmail.com', user_game_id: courseRows[2].id },
    ], {});
  },

  down: async (queryInterface) => {
    await queryInterface.bulkDelete('user_game_biodata', null, {});
    await queryInterface.bulkDelete('user_games', null, {});
  }
};
