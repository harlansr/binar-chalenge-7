const router = require("express").Router()
const { Home, User, ApiController, AccountController } = require("../controller")
const Middleware = require("../middleware")

const userRouter = require("./userRouter");
const apiRouter = require("./apiRouter");

router.use(userRouter);
router.use(apiRouter);

// Router
router.get("/", Home.home)
router.get("/game", Middleware.doWithAuth, Home.game)

module.exports = router