const router = require("express").Router()
const { Home, User, ApiController, AccountController } = require("../controller")
const Middleware = require("../middleware")
// const passport = require("../lib/passport")
const passport = require('passport')
// console.log(passport);
router.get("/login", Middleware.doWithoutAuth, User.loginPage)
// router.post("/login", Middleware.doWithoutAuth, User.login)
router.post('/login', passport.authenticate('local', {
    successRedirect: '/',
    failureRedirect: '/login?failed=true',
    failureFlash: true
}))

router.get('/whoami', Middleware.doWithAuth, User.whoami)

router.get("/register", Middleware.doWithoutAuth, User.registerPage)
router.post("/register", Middleware.doWithoutAuth, User.register)
router.get("/logout", Middleware.doWithAuth, User.logout)

router.get("/account/edit", Middleware.doWithAuth, AccountController.account_edit)
router.post("/account/edit", Middleware.doWithAuth, AccountController.account_edit_post)
router.get("/account/history", Middleware.doWithAuth, AccountController.history)
router.get("/account/history/delete/:history_id", Middleware.doWithAuth, AccountController.history_delete)

module.exports = router